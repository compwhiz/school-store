@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ledger: {{ $group->group }}</h4>
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex flex-row">
                                        <div class="round align-self-center round-success">
                                            <i class="ti-id-badge"></i></div>
                                        <div class="m-l-10 align-self-center">
                                            <h3 class="m-b-0">{{ $group->items->count() }}</h3>
                                            <h5 class="text-muted m-b-0">
                                                Total {{ \Illuminate\Support\Str::plural('Items',$group->count()) }}</h5>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">{{ $group->group }} Items</h4>
                    {{--<button type="button" class="btn btn-dark d-none d-lg-block m-l-15 pull-right" data-toggle="modal"--}}
                    {{--data-target=".additem">--}}
                    {{--<i class="fa fa-plus-circle"></i> Create New--}}
                    {{--</button>--}}

                    <div class="modal fade additem" tabindex="-1" role="dialog"
                         aria-labelledby="myLargeModalLabel" aria-hidden="true"
                         style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myLargeModalLabel">Add Item to {{ $group->group }}</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <additem :Onegroup="{{$group}}"></additem>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger waves-effect text-left"
                                            data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>


                    <items :allitems="{{ $group->items }}"></items>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Issued {{ $group->group }}</h4>
                    <issue :issues="{{ $group->issued }}"></issue>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Received {{ $group->group }}</h4>
                    <receive :items="{{ $group->received  }}"></receive>

                </div>
            </div>

        </div>
    </div>
@endsection
