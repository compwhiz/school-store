@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Items</h4>
                    <div class="pull-right mb-4">
                        <button type="button" class="btn btn-dark d-none d-lg-block m-l-15" data-toggle="modal"
                                data-target=".bs-example-modal-lg">
                            <i class="fa fa-plus-circle"></i> Create Item
                        </button>

                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
                             aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myLargeModalLabel">Add Item</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <additem :groupings="{{ \App\Group::all() }}"></additem>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger waves-effect text-left"
                                                data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                    <div class="table-responsive m-t-40">
                        <items :allitems="{{ \App\Item::with('group','received','issued')->get() }}"></items>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
