<html>
<head>
    <title>LOCAL PURCHASE ORDER</title>
    <script language="JavaScript" type="text/javascript">
        /*setTimeout("window.print();", 10000);*/
    </script>
    <style>
        body {
            padding: 0px;
            margin: 0px;
            font-size: 12px;
        }

        table.data {
            font-family: Verdana;
            font-size: 17px;
            empty-cells: show;
            border: 1px solid black;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        table.purchase {
            font-family: "Times New Roman";
            font-size: 17px;
            empty-cells: show;
            border: none;
            /*padding-left: 10px !important;*/

            margin: 24px !important;
            border-collapse: collapse;
            border-spacing: 0.5rem;
        }

        table.data td {
            border: 1px solid black;
        }

        table.data td.header {
            background-color: #EDECEB;
            font-size: medium;
        }

        table.data td.abottom {
            vertical-align: bottom;
            font-size: 10px;
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px;
            height: 50px;
        }

        .pass {
            /*background-color: black !important;*/
            color: black !important;
            padding: 2px;
            font-weight: bold;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }


    </style>

</head>
<body>
<table cellpadding="2" cellspacing="0" width="100%" class="data">
    <tr>
        <td colspan="9">

            <table width="100%" border=0 cellspacing="0" cellpadding="1" class="data">

                <tr>
                    <td rowspan="6">
                        <img src="{{ asset('makinduHigh.PNG') }}" width="90px">
                    </td>
                </tr>
                <tr>
                    <td valign="top" colspan="2">
                        <strong>MAKINDU BOYS HIGH SCHOOL</strong>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>ADDRESS : P.O BOX 28 &mdash; 90138, MAKINDU.</b></td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>Tel : 0713 642 175</b></td>
                </tr>

                <tr>
                    <td colspan="2" class="title"><b>Vision : </b></td>
                </tr>

                <tr>
                    <td colspan="4" class="title"><strong>&nbsp;LOCAL PURCHASE ORDER</strong></td>
                </tr>

            </table>

        </td>
    </tr>
</table>
<table cellpadding="2" cellspacing="0" width="100%" style="margin-top: 20px !important;">

    <tr nobr="true">
        <td nowrap align="center" valign="center" style="border: 1px transparent solid !important;">
            <h2>&nbsp;LOCAL PURCHASE ORDER</h2></td>
    </tr>

    <table cellpadding="2" cellspacing="0" width="100%" class="purchase">
        <tr style="font-size: 24px">
            <td nowrap class="header">TO:
                <span class="pass">{{ $items->contractors->name ?? '' }},<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P.O BOX {{ $items->contractors->phonenumber ?? '' }}</span>
            </td>
            <td nowrap class="header">LPO NO:
                <span class="pass">{{ $items->lponumber }}</span>
            </td>
        </tr>
        <tr style="font-size: 24px;padding-top: 80px !important;">
            <td nowrap class="header" style="color:transparent !important;">LPO NO:
            </td>
            <td nowrap class="header">Date:
                <span class="pass">{{ \Carbon\Carbon::parse($items->date)->toFormattedDateString() }}</span>
            </td>
        </tr>
    </table>
    <table cellpadding="2" cellspacing="0" width="100%" class="purchase">
        <tr>
            <td nowrap class="header" style="font-size: 24px">
                <p>Please deliver the following goods to <strong>MAKINDU BOYS HIGH SCHOOL</strong>
                    on or before <strong>{{ \Carbon\Carbon::parse($items->supplyby)->toFormattedDateString() }}</strong>
                    and
                    <br>submit invoices without delay to <strong>MAKINDU BOYS HIGH SCHOOL</strong>

                </p>

            </td>
        </tr>

    </table>

    <table cellpadding="2" cellspacing="0" width="100%" class="data">

        <tr nobr="true">
            <td nowrap class="header">Item No.</td>
            <td nowrap class="header">Description of Goods</td>
            <td nowrap class="header">Unit</td>
            <td nowrap class="header">Quantity</td>
            <td class="header">Price in Kshs &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        @foreach($items->items as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->item }}</td>
                <td>{{ $item->unit }}</td>
                <td>{{ $item->quantity }}</td>
                <td align="left"><strong>{{ number_format($item->price,2) }}</strong></td>
            </tr>
        @endforeach


        <tr>
            <td><strong>TOTAL</strong></td>
            <td colspan="3"></td>
            <td style="border-bottom: double 6px black !important;">
                <strong>{{ number_format($items->items()->sum('price'),2) }}</strong></td>
        </tr>
    </table>
    <table cellpadding="2" cellspacing="0" width="100%" class="purchase"
           style="padding-top: 100px !important;margin-top: 100px !important;">
        <tr>
            <td nowrap class="header"><strong>Prepared By</strong>: ___________________________________________</td>
            <td nowrap class="header"><strong>VoteHead</strong>: ___________________________________________</td>
        </tr>
        <tr>
            <td nowrap class="header"><strong>Signature</strong>: ___________________________________________</td>
            <td nowrap class="header"><strong>Date</strong>: ___________________________________________</td>
        </tr>
        <tr>
            <td nowrap class="header" colspan="2"><strong>Designation</strong>:
                ___________________________________________
            </td>
        </tr>


    </table>
    <table cellpadding="2" cellspacing="0" width="100%" class="purchase"
           style="padding-top: 50px !important;margin-top: 50px !important;">
        <tr>
            <td nowrap class="header" colspan="3"><strong><em>Approved By:</em></strong></td>
        </tr>
        <tr>
            <td>Head of Institution:
                <br><br>...............................................................
            </td>
            <td>Date: <br><br> _______________________________________________</td>
        </tr>
        <tr>
            <td style="margin-top: 50px !important;padding-top: 50px !important;" colspan="2">Signature: <br> <br>................................................................
            </td>

        </tr>

    </table>
</table>
<footer>
    <table cellpadding="2" cellspacing="0" width="100%" class="purchase">

        <tr>
            <td>
                <p><strong><em>I confirm that funds are available and that the commitment has been noted in the
                            Commitments Register/Vote
                            Book</em></strong></p>
            </td>
        </tr>
    </table>
</footer>
</body>
</html>
