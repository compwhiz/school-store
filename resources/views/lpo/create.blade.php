@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add LPO</h4>
                    <div class="col-md-12">
                        <addlpo :items="{{ \App\Item::all() }}" :suppliers="{{ \App\Supplier::all() }}"></addlpo>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">LPO</h4>
                </div>
                <lpos :lpos="{{ \App\LPO::with('items','contractors')->get() }}"></lpos>
            </div>

        </div>
    </div>
@endsection
