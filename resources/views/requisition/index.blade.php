@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Requisitions</h4>
                    <div class="table-responsive m-t-40">
                        <requisitions :requisitions="{{ \App\Requistion::with('items')->get() }}"></requisitions>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
