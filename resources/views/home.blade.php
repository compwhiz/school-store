@extends('layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="card">
            <div class="p-20 p-t-25">
                <h4 class="card-title">Issued Items</h4>
            </div>
            <items :allitems="{{ \App\Item::with('group','received','issued')->get() }}"></items>
            <receive :items="{{ \App\Receive::with(['supplier','item'])->get() }}"></receive>

        </div>

    </div>

@endsection
