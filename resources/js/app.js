
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');

window.Bus = new Vue({});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('addgroup', require('./components/group/create'));
Vue.component('groups', require('./components/group/index'));
Vue.component('editgroup', require('./components/group/edit'));
Vue.component('additem', require('./components/item/create'));
Vue.component('items', require('./components/item/index'));
Vue.component('receiveitems', require('./components/receive/receive'));
Vue.component('receive', require('./components/receive/index'));
Vue.component('supplier', require('./components/supplier/index'));
Vue.component('addsupplier', require('./components/supplier/create'));
Vue.component('issue', require('./components/issue/index'));
Vue.component('issueitem', require('./components/issue/create'));
Vue.component('addinventory', require('./components/inventory/create'));
Vue.component('receiveinventory', require('./components/inventory/receive'));
Vue.component('inventory', require('./components/inventory/index'));
Vue.component('addrequisition', require('./components/requisition/create'));
Vue.component('requisitions', require('./components/requisition/index'));
Vue.component('addlpo', require('./components/lpo/create'));
Vue.component('lpos', require('./components/lpo/index'));

const app = new Vue({
    el: '#app'
});
