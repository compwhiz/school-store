<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;


    public function group()
    {
        return $this->hasOne(Group::class, 'id', 'group_id');
    }

    public function received()
    {
        return $this->hasMany(Receive::class, 'item_id', 'id');
    }

    public function issued()
    {
        return $this->hasMany(Issue::class, 'item', 'id');
    }

    public function items()
    {
        return $this->hasMany(Issue::class, 'item', 'id');
    }
}
