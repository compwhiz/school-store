<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LPO;

class Supplier extends Model
{
    public function lpo()
    {
        return $this->hasMany(LPO::class,'contractor','id');
    }
}
