<?php

namespace App\Http\Controllers;

use App\LPO;
use App\lpoitem;
use Illuminate\Http\Request;

class LPOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('lpo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lpo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, LPO $lpo)
    {
        $this->validate($request, [
            'lponumber'  => ['required', 'unique:lpo,lponumber'],
            'contractor' => ['required'],
            'date'       => ['required'],
            'supplyby'   => ['required'],
        ]);
        $lpo->lponumber = $request->lponumber;
        $lpo->contractor = $request->contractor;
        $lpo->date = $request->date;
        $lpo->supplyby = $request->supplyby;
        $lpo->saveOrFail();

        collect($request->lpos)->map(function ($item) use ($lpo) {
            $item = new lpoitem([
                'quantity' => $item['quantity'], 'item' => $item['item'], 'unit' => $item['Unit'],'price' => $item['price']
            ]);
            $lpo->items()->save($item);
        });
        return response()->json($lpo);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LPO $lPO
     * @return \Illuminate\Http\Response
     */
    public function show(LPO $lPO)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LPO $lPO
     * @return \Illuminate\Http\Response
     */
    public function edit(LPO $lPO)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\LPO $lPO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LPO $lPO)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LPO $lPO
     * @return \Illuminate\Http\Response
     */
    public function destroy(LPO $lPO)
    {
        //
    }

    public function print(LPO $lpo)
    {
        $items = $lpo->load('items','contractors');

        return view('lpo.lpo.index',compact('items'));
    }
}
