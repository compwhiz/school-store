<?php

namespace App\Http\Controllers;

use App\Issue;
use App\Item;
use Illuminate\Http\Request;

class IssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Item $item)
    {
        return view('issue.index', ['items' => $item::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('issue.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Issue $issue)
    {
        $this->validate($request, [
            'date'     => ['required'],
            'quantity' => ['required'],
            'item'     => ['required'],
            'issuedto' => ['required'],
        ]);
        $issue->date = $request->date;
        $issue->item = $request->item;
        $issue->quantity = $request->quantity;
        $issue->issuedto = $request->issuedto;
        $issue->saveOrFail();

        return response()->json($issue);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Issue $issue
     * @return \Illuminate\Http\Response
     */
    public function show(Issue $issue)
    {
        return view('issue.show', [
            'issue' => $issue->load('issued.items')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Issue $issue
     * @return \Illuminate\Http\Response
     */
    public function edit(Issue $issue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Issue $issue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Issue $issue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Issue $issue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Issue $issue)
    {
        //
    }
}
