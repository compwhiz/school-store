<?php

namespace App\Http\Controllers;

use App\RequisitionItem;
use App\Requistion;
use Illuminate\Http\Request;

class RequistionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('requisition.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('requisition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Requistion $requistion)
    {
        $this->validate($request, [
            'date'        => ['required'],
            'department'  => ['required'],
            'account'     => ['required'],
            'requestedby' => ['required'],
        ]);

        $requistion->date = $request->date;
        $requistion->department = $request->department;
        $requistion->account = $request->account;
        $requistion->requestedby = $request->requestedby;
        $requistion->saveOrFail();
        $id = [];
        collect($request->requisitions)->map(function ($item, $key) use ($requistion, &$id) {
            $requistionItem = new RequisitionItem([
                'quantity' => $item['quantity'], 'item' => $item['item'], 'unit' => $item['Unit']
            ]);
            $id[] = $requistion;
            $requistion->items()->save($requistionItem);
        });

        return response()->json($requistion);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requistion $requistion
     * @return \Illuminate\Http\Response
     */
    public function show(Requistion $requistion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requistion $requistion
     * @return \Illuminate\Http\Response
     */
    public function edit(Requistion $requistion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Requistion $requistion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requistion $requistion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requistion $requistion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requistion $requistion)
    {
        //
    }

    public function print(Requistion $requistion)
    {
        return view('requisition.print.index',['requistion'=>$requistion->load('items')]);
    }
}
