<?php

namespace App\Http\Controllers;

use App\Receive;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ReceiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier = Receive::with('supplier')->get()->groupBy('date')->map(function (Collection $collection) {
            return $collection->groupBy('lpo_id');
        });
        return view('receive.index', compact('supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request, Receive $receive)
    {
//        $this->validate($request, [
//            'date'     => ['date', 'required'],
//            'item'     => ['numeric', 'required'],
//            'quantity' => ['numeric', 'required'],
//            'from'     => ['numeric', 'required'],
//        ]);

        collect($request->itemse)->each(function ($item) use ($request) {
            $receive = new Receive();
            $receive->date = $request->date;
            $receive->item_id = $item['item'];
            $receive->quantity = $item['received'];
            $receive->lpo_id = $item['lpo_id'];
            $receive->supplier_id = $request->from;
            $receive->saveOrFail();
        });


        return response()->json($receive->load(['item', 'supplier']));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Receive $receive
     * @return \Illuminate\Http\Response
     */
    public function show(Receive $receive)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Receive $receive
     * @return \Illuminate\Http\Response
     */
    public function edit(Receive $receive)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Receive $receive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Receive $receive)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Receive $receive
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receive $receive)
    {
        //
    }

    public function print($date, $supplier, $lpo)
    {
        $items = Receive::with('item', 'supplier')->where('date', $date)
            ->where('supplier_id', $supplier)
            ->where('lpo_id', $lpo)->get();

        return view('receive.print.index',compact('items'));
    }
}
