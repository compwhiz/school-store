<?php

namespace App\Http\Controllers;

use App\RequisitionItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequisitionItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequisitionItem  $requisitionItem
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionItem $requisitionItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequisitionItem  $requisitionItem
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionItem $requisitionItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequisitionItem  $requisitionItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionItem $requisitionItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequisitionItem  $requisitionItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionItem $requisitionItem)
    {
        //
    }
}
