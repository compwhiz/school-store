<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

class EnsureCompletePayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->downForPayment()) {
            throw new ServiceUnavailableHttpException(503, 'Please note your trial period is over. Contact 0713 642 175', null, 503,
                []);
        }
        return $next($request);
    }

    protected function downForPayment()
    {
//        if (config('licence.trial_date') === '2018-6-10' || config('licence.trial_date') < '2018-6-10') {
//            return true;
//        }
        return false;

    }
}
