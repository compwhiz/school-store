TODO: Update my library system to capture Kenya ISBNS
 - 9789966103314
# Purchase Requistion

# Local Purchase Order

- Name of Contractor
- Date before supply
    - Item No
    - Description
    - Unit
    - Quantity
    - Price
- Voteheads
- LPO Number
- LPO is linked to Deliveries by Contractor.

#Delivery

# Re-order Levels for every item. 
- Default is 50 Items

# Delivering
- Is linked to LPO

# Goods Received Note
- If contractor has no Delivery Note
- Make it printable

# Asset Register
- CRUD on Asset Register

# Inventory Book
-  Groups e.g Library, Kitchen
-  Issued to Departments
-  Withdrawals

#Damages
- Can only occur on Asset Register Items

# Returning spoilt Goods and Assets
- Return some spoilt goods against received items

