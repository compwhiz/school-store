<?php

use Faker\Generator as Faker;

$factory->define(App\Group::class, function (Faker $faker) {
    return [
        'group' => $faker->randomElements([['Detergent', 'Foodstuff']])
    ];
});
