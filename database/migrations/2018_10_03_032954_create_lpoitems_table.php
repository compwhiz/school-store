<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpoitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lpoitems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quantity');
            $table->string('unit');
            $table->string('item');
            $table->integer('price');
            $table->unsignedInteger('lpo_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lpoitems');
    }
}
